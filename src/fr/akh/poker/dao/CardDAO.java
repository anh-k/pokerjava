package fr.akh.poker.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author HUYNH Anh-Khoa
 */
public class CardDAO {


    private BufferedReader pokerFile;
    private ArrayList<String> stringsInFile;

    public CardDAO() {
        try {
            stringsInFile = new ArrayList<>();
            String line;
            ClassLoader classLoader = getClass().getClassLoader();
            pokerFile = new BufferedReader(new FileReader(classLoader.getResource("poker.txt").getFile()));

            while ((line = pokerFile.readLine()) != null) {
                stringsInFile.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<String> getStringsInFile() {
        return stringsInFile;
    }

}





