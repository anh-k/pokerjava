package fr.akh.poker.job;

/**
 * @author HUYNH Anh-Khoa
 */
public class Card implements Comparable {

    private Integer rankValue;
    private String colorValue;

    public Card(Character rank, Character color) {
        setRank(rank);
        setColor(color);
    }

    // Full name of a color's character
    private void setColor(Character color) {

        if (color.equals('H'))
            colorValue = "Hearts";
        else if (color.equals('D'))
            colorValue = "Diamonds";
        else if (color.equals('S'))
            colorValue = "Spades";
        else if (color.equals('C'))
            colorValue = "Clubs";

    }

    // Transform a given character into a value
    public void setRank(Character rank) {

        Integer value;

        if (rank.equals('T'))
            value = 10;
        else if (rank.equals('J'))
            value = 11;
        else if (rank.equals('Q'))
            value = 12;
        else if (rank.equals('K'))
            value = 13;
        else if (rank.equals('A'))
            value = 14;

        else
            value = Integer.parseInt(String.valueOf(rank));

        rankValue = value;

    }

    public String getColorValue() {
        return colorValue;
    }


    public Integer getRankValue() {


        return rankValue;
    }


    @Override
    public String toString() {

        String rankToString = "";

        if (rankValue == 11)
            rankToString = "Jack";
        else if (rankValue == 12)
            rankToString = "Queen";
        else if (rankValue == 13)
            rankToString = "King";
        else if (rankValue == 14)
            rankToString = "Ace";

        else
            rankToString = String.valueOf(rankValue);

        return rankToString + " of "
                + colorValue;
    }

    @Override
    public int compareTo(Object o) {
        Card other = (Card) o;
        return rankValue - other.getRankValue();

    }
}
