package fr.akh.poker.job;

import java.util.ArrayList;

/**
 * @author HUYNH Anh-Khoa
 */
public class Hand {

    private ArrayList<Card> cards;

    public Hand() {
        this.cards = new ArrayList<>();
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public void addCard(Card card) {

        this.cards.add(card);
    }

    public void removeCard(Card card) {

        this.cards.remove(card);
    }

    @Override
    public String toString() {
        return String.valueOf(cards);
    }
}
