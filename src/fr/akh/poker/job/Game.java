package fr.akh.poker.job;

import fr.akh.poker.utilities.HandComparator;
import fr.akh.poker.utilities.HandGenerator;

/**
 * @author HUYNH Anh-Khoa
 */
public class Game {

    private HandGenerator handGenerator;
    private HandComparator playerOneHand;
    private HandComparator playerTwoHand;
    private int winnerNumber;
    private String stringInFile;

    public Game(String stringInFile) {
        this.stringInFile = stringInFile;
    }

    public void playANewGame() {

        generateHandsFromFile(stringInFile);
        evaluateHand();
        setWinnerOfTheGame();
    }


    private void generateHandsFromFile(String stringInFile) {

        handGenerator = new HandGenerator(stringInFile);
        handGenerator.generateTheHands();
    }

    // Get poker hands from both players in each game
    private void evaluateHand() {

        playerOneHand = new HandComparator(handGenerator.getFirstHand(), 1);
        playerTwoHand = new HandComparator(handGenerator.getSecondHand(), 2);
    }

    private void setWinnerOfTheGame() {
        int result = playerOneHand.compareTo(playerTwoHand);

        if (result > 0) {
            System.out.println("Player one wins with " + "'" + playerOneHand.getPokerHandToString() + "' !");
            winnerNumber = 1;
        } else {
            System.out.println("Player two wins with " + "'" + playerTwoHand.getPokerHandToString() + "' !");
            winnerNumber = 2;
        }

    }

    public int getWinnerNumber() {
        return winnerNumber;
    }

}
