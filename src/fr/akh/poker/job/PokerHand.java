package fr.akh.poker.job;

/**
 * @author HUYNH Anh-Khoa
 */
public class PokerHand {

    private String name;
    private int rank;
    private int highestValue;

    public PokerHand(int rank, int highestValue) {

        this.rank = setRank(rank);
        this.highestValue = highestValue;
    }

    public int getRank() {
        return rank;
    }

    public int setRank(int rank) {

        if (rank == 0)
            name = "High card";
        else if (rank == 1)
            name = "One pair";
        else if (rank == 2)
            name = "Two pairs";
        else if (rank == 3)
            name = "Three of a Kind";
        else if (rank == 4)
            name = "Straight";
        else if (rank == 5)
            name = "Flush";
        else if (rank == 6)
            name = "Full House";
        else if (rank == 7)
            name = "Four of a Kind";
        else if (rank == 8)
            name = "Straight Flush";
        else if (rank == 9)
            name = "Royal Flush";

        return rank;
    }

    public int getHighestValue() {
        return highestValue;
    }

    public void setHighestValue(int highestValue) {

        this.highestValue = highestValue;
    }

    public String getName() {
        return name;
    }

}
