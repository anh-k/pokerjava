package fr.akh.poker.utilities;

import fr.akh.poker.job.Card;
import fr.akh.poker.job.Hand;
import fr.akh.poker.job.PokerHand;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author HUYNH Anh-Khoa
 */
public class HandComparator implements Comparable {
    private ArrayList<Card> cardsList;
    private PokerHand pokerHand;
    private int playerNumber;
    private int valueMaxOccurrenced;
    String pokerHandToString;

    public HandComparator(Hand handOfPlayer, int playerNumber) {
        this.cardsList = handOfPlayer.getCards();
        cardsList.sort(Card::compareTo);
        this.playerNumber = playerNumber;
        defineNumberOfOccurrences();
        getHighCard();
    }

    private HashMap<Integer, Integer> defineNumberOfOccurrences() {
        HashMap<Integer, Integer> numberOfOccurrences = new HashMap<>();
        int maxNumberOfOccurrences = 0;

        for (Card card : cardsList) {

            if (!numberOfOccurrences.containsKey(card.getRankValue()))
                numberOfOccurrences.put(card.getRankValue(), 1);
            else
                numberOfOccurrences.put(card.getRankValue(), numberOfOccurrences.get(card.getRankValue()) + 1);

            if (numberOfOccurrences.get(card.getRankValue()) > maxNumberOfOccurrences) {
                maxNumberOfOccurrences = numberOfOccurrences.get(card.getRankValue());
                valueMaxOccurrenced = card.getRankValue();

            }

        }

        return numberOfOccurrences;
    }

    private boolean isRoyal() {
        boolean isRoyal = false;
        if (cardsList.get(0).getRankValue() == 10 && cardsList.get(1).getRankValue() == 11 && cardsList.get(2).getRankValue() == 12 && cardsList.get(3).getRankValue() == 13 && cardsList.get(4).getRankValue() == 14) {

            isRoyal = true;
        }
        return isRoyal;
    }

    private boolean isRoyalFlush() {

        if (isRoyal() && isFlush()) {
            pokerHand.setRank(9);

            return true;
        } else
            return false;

    }

    private boolean isStraightFlush() {

        if (isStraight() && isFlush()) {
            pokerHand.setRank(8);

            return true;
        } else
            return false;

    }


    private boolean isFourOfAKind() {

        if (defineNumberOfOccurrences().get(valueMaxOccurrenced) == 4) {
            pokerHand = new PokerHand(7, valueMaxOccurrenced);

            return true;
        } else
            return false;
    }

    private boolean isFlush() {
        boolean isFlush = true;
        for (int i = 0; i < cardsList.size() - 1; i++)
            if (!cardsList.get(i).getColorValue().equals(cardsList.get(i + 1).getColorValue()))
                isFlush = false;
            else {
                pokerHand = new PokerHand(6, valueMaxOccurrenced);


            }
        return isFlush;
    }

    private boolean isFullHouse() {
        if (isThreeOfAKind() && isOnePair()) {
            pokerHand.setRank(5);

            return true;
        } else
            return false;

    }

    private boolean isStraight() {
        boolean isStraight = true;
        int maxValue = 0;

        for (int i = 0; i < cardsList.size() - 1; i++) {
            if (isStraight && cardsList.get(i).getRankValue() != cardsList.get(i + 1).getRankValue() - 1)
                isStraight = false;

            else if (cardsList.get(i).getRankValue() > maxValue)
                maxValue = cardsList.get(i).getRankValue();
        }

        if (isStraight)
            pokerHand = new PokerHand(4, maxValue);


        return isStraight;
    }

    private boolean isThreeOfAKind() {
        if (defineNumberOfOccurrences().get(valueMaxOccurrenced) == 3) {
            pokerHand = new PokerHand(3, valueMaxOccurrenced);

            return true;
        } else
            return false;

    }

    private boolean isTwoPairs() {

        boolean isTwoPairs = false;
        int maxValue = 0;
        int numberOfPairs = 0;
        HashMap<Integer, Integer> numberOfOccurrences = new HashMap<>(defineNumberOfOccurrences());
        for (Card card : cardsList) {
            if (numberOfOccurrences.get(card.getRankValue()) != null && numberOfOccurrences.get(card.getRankValue()) == 2) {
                numberOfPairs++;
                numberOfOccurrences.remove(card.getRankValue());

                if (card.getRankValue() > maxValue)
                    maxValue = card.getRankValue();
            }

            if (numberOfPairs == 2) {
                pokerHand = new PokerHand(2, maxValue);

                isTwoPairs = true;
            }
        }
        return isTwoPairs;

    }

    private boolean isOnePair() {

        boolean isOnePair = false;
        int maxValue = 0;
        int numberOfPairs = 0;
        HashMap<Integer, Integer> numberOfOccurrences = new HashMap<>(defineNumberOfOccurrences());
        for (Card card : cardsList) {
            if (numberOfOccurrences.get(card.getRankValue()) != null && numberOfOccurrences.get(card.getRankValue()) == 2) {
                numberOfPairs++;
                numberOfOccurrences.remove(card.getRankValue());

                if (card.getRankValue() > maxValue)
                    maxValue = card.getRankValue();
            }

            if (numberOfPairs == 1) {
                pokerHand = new PokerHand(1, maxValue);

                isOnePair = true;
            }
        }
        return isOnePair;

    }

    private void getHighCard() {

        int highestValue = 0;

        for (int i = 0; i < cardsList.size() - 1; i++) {

            if (highestValue < cardsList.get(i).getRankValue()) {
                highestValue = cardsList.get(i + 1).getRankValue();
            }
        }
        if (!isRoyalFlush() && !isStraightFlush() && !isFourOfAKind() && !isFullHouse() && !isFlush() && !isStraight() && !isThreeOfAKind() && !isTwoPairs() && !isOnePair())
            pokerHand = new PokerHand(1, 0);


    }

    public String getPokerHandToString() {
        this.pokerHandToString = pokerHand.getName();
        return pokerHandToString;
    }


    @Override
    public int compareTo(Object o) {

        HandComparator otherHand = (HandComparator) o;
        if (pokerHand.getRank() != otherHand.pokerHand.getRank())
            return (pokerHand.getRank() - otherHand.pokerHand.getRank());

        else if (pokerHand.getHighestValue() != otherHand.pokerHand.getHighestValue()) {

            return (pokerHand.getHighestValue() - otherHand.pokerHand.getHighestValue());
        } else
            return (cardsList.get(cardsList.size() - 1).getRankValue() - otherHand.cardsList.get(otherHand.cardsList.size() - 1).getRankValue());


    }
}
