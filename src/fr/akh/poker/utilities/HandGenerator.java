package fr.akh.poker.utilities;

import fr.akh.poker.job.Card;
import fr.akh.poker.job.Hand;

/**
 * @author HUYNH Anh-Khoa
 */
public class HandGenerator {


    private Hand firstHand;
    private Hand secondHand;
    private String lineInFile;

    public HandGenerator(String lineInFile) {
        this.lineInFile = lineInFile;
        firstHand = new Hand();
        secondHand = new Hand();

    }

    // Generate hands of both players from the file's lines
    public void generateTheHands() {

        int separator = 0;

        for (int i = 0; i < lineInFile.length(); i = i + 3) {
            Card newCard = new Card(lineInFile.charAt(i), lineInFile.charAt(i + 1));
            separator++;
            if (separator <= 5)
                firstHand.addCard(newCard);

            else
                secondHand.addCard(newCard);
        }


        System.out.println("Player 1 : " + firstHand);
        System.out.println("Player 2 : " + secondHand);
    }

    public Hand getFirstHand() {
        return firstHand;
    }

    public void setFirstHand(Hand firstHand) {
        this.firstHand = firstHand;
    }

    public Hand getSecondHand() {
        return secondHand;
    }

    public void setSecondHand(Hand secondHand) {
        this.secondHand = secondHand;
    }
}


