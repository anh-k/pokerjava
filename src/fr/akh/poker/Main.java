package fr.akh.poker;

import fr.akh.poker.dao.CardDAO;
import fr.akh.poker.job.Game;

import java.io.FileNotFoundException;
import java.util.Iterator;

/**
 * @author HUYNH Anh-Khoa
 */

public class Main {

    private static int playerOneNumberOfWins;

    public static void main(String[] args) throws FileNotFoundException {


        generateGameFromAFile();


    }

    private static void generateGameFromAFile() throws FileNotFoundException {
        CardDAO cardDao = new CardDAO();
        Iterator iterator = cardDao.getStringsInFile().iterator();

        int gameNumber = 1;

        while (iterator.hasNext()) {
            String gameNumberToString = "\n Game number " + gameNumber + ":";
            System.out.println(gameNumberToString);

            Game newGame = new Game(iterator.next().toString());

            newGame.playANewGame();
            gameNumber++;

            System.out.println("\n --------------------------------------");
            countPlayerOneWins(newGame.getWinnerNumber());


        }
        System.out.println();
        System.out.println("Final result : ");
        System.out.println("Player one has won " + playerOneNumberOfWins + " times !");
    }

    private static void countPlayerOneWins(int numberOfWins) {

        if (numberOfWins == 1)
            playerOneNumberOfWins++;
    }
}
